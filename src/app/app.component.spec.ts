import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { MapService } from './services/maps/maps.service';
import { expand } from 'rxjs/operators/expand';
import { GeoCoordinates } from './models/geocoordinates.model';
import { HttpClientModule }    from '@angular/common/http';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      providers: [
        MapService
      ],
      imports: [
        HttpClientModule
      ],
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should set error message on set call', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;

    app.setErrorMessage("Test");
    expect(app.errorMessage).toEqual("Test");
  }));

  it('should set coordinates object correctly', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;

    var testLatAndLng = { lat: 22, lng: -10 };
    var expectedOutcome: GeoCoordinates = { lat: 22, lng: -10 };

    app.setLatAndLng(testLatAndLng);

    expect(app.coords).toEqual(expectedOutcome);
  }));
});

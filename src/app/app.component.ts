import { Component, OnInit } from '@angular/core';
import { MapService } from './services/maps/maps.service';
import { GeoCoordinates } from './models/geocoordinates.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  public address: any = "";
  public nLocation: object;
  public errorMessage: string;

  coords: GeoCoordinates = {
    lat: null,
    lng: null
  };

  constructor(private _mapService: MapService) {
  }

  private setErrorMessage(message) {
    this.errorMessage = message;
  }

  private setLatAndLng(nLocation) {
    this.coords.lng = nLocation.lng;
    this.coords.lat = nLocation.lat;
  }

  private resetLatAndLng() {
    this.coords.lng = null;
    this.coords.lat = null;
  }

  public getGeolocation() {
    this._mapService.geolocationLookup(this.address).subscribe(result => {
      switch(result["status"]){
        case "OK":
          this.setErrorMessage("");
          this.setLatAndLng(result["results"][0].geometry.location);
          break;
        case "ZERO_RESULTS":
          this.setErrorMessage("There are no results for that address. That could be because it is invalid or has not been spelled correctly. Please try again.");
          this.resetLatAndLng();
          break;
        case "INVALID_REQUEST":
          this.setErrorMessage("Please enter a correct address into the address field.");
          this.resetLatAndLng();
          break;
      }
    })
  }
}

import { Injectable } from '@angular/core';
import { AgmCoreModule, MapsAPILoader } from '@agm/core';
import { GeoCoordinates } from '../../models/geocoordinates.model';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import { defer } from 'q';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()

export class MapService {
  private geocoder : any;
  private apiKey: string = "AIzaSyBW8bzHlb6mKHRxjrW0WUDTWFYnukCSB2I";
  private gmapsApiUrl: string = "https://maps.googleapis.com/maps/api/geocode/json";

  constructor(private http: HttpClient) {}

  private buildUrl(nAddress) {
    var formattedAddress = nAddress.replace(/ /g, '+');
    return this.gmapsApiUrl + '?address=' + formattedAddress + '&key=' + this.apiKey;
  }

  private handleError<T>(result?: T) {
    return (error: any): Observable<T> => {
      return of(result as T);
    }
  }

  geolocationLookup(nAddress): Observable<Object> {
    return this.http.get(this.buildUrl(nAddress))
                    .pipe(catchError(this.handleError({
                      status: "INVALID_REQUEST"
                    })));
  }
}

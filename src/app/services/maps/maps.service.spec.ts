import { TestBed, async } from '@angular/core/testing';
import { MapService } from './maps.service';
import { HttpClientModule }    from '@angular/common/http';

describe('MapService', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HttpClientModule
      ]
    }).compileComponents();
  }));
});

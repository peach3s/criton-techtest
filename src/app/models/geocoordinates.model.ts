export class GeoCoordinates {
  lat: number;
  lng: number;
}
